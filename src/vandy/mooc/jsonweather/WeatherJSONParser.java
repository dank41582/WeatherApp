package vandy.mooc.jsonweather;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vandy.mooc.aidl.WeatherData;

import android.util.Log;

/**
 * Parses the Json weather data returned from the Weather Services API
 * and returns a List of JsonWeather objects that contain this data.
 */
public class WeatherJSONParser {
	
	private static String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?q=";
	
    /**
     * Used for logging purposes.
     */
    private final static String TAG = "WeatherJSONParser";
    
    
	//get the JSON results from the URL, demarshal the JSON and return the resulting java objects
    public static List<WeatherData> getWeatherData(String city) {
    	
    	Log.d(TAG, "parse Json for city "+city+"...");
    	
    	//append city parameter to base Url
    	String url = BASE_URL + city;
    	
    	//get json from url
    	String jsonString = getDataFromUrl(url);
    	
    	final List<WeatherData> weatherDataList = new ArrayList<WeatherData>(0);
    	
    	//if city wasn't found, return an empty array
    	if (jsonString.equals("{\"message\":\"Error: Not found city\",\"cod\":\"404\"}")){
    		return weatherDataList;
    	}
    	
    	//otherwise, parse json to get java objects
    	List<JsonWeather> jsonWeatherArrayList = null;
		try {
			jsonWeatherArrayList = parseJson(jsonString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		if (jsonWeatherArrayList != null){
			
	    	//convert JsonWeather objects to WeatherData objects which can be passed between processes
	    	for (JsonWeather jsonWeather : jsonWeatherArrayList){
	    		
	    		weatherDataList.add(new WeatherData(
	    				jsonWeather.getName(),
	    				jsonWeather.getWind().getSpeed(),
	    				jsonWeather.getWind().getDeg(),
	    				jsonWeather.getMain().getTemp(),
	    				jsonWeather.getMain().getHumidity(),
	    				jsonWeather.getSys().getSunrise(),
	    				jsonWeather.getSys().getSunset(),
	    				System.currentTimeMillis()
	    				));
	    	}
		}
		
    	return weatherDataList;
    }
    
    //go to URL and get resulting JSON string - return this string
    private static String getDataFromUrl(String url) {
    	
        HttpURLConnection con = null ;
        InputStream is = null;
 
        try {
        	
        	//open HTTP connection
            con = (HttpURLConnection) new URL(url).openConnection();
            
            //set HTTP request params
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);
            
            //send HTTP request
            con.connect();
             
            //get HTTP response from inputstream
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            
            //process HTTP response
            StringBuffer buffer = new StringBuffer();
            String line = null;
            while ((line = br.readLine()) != null )
            	buffer.append(line);
             
            //close connection and inputstream
            is.close();
            con.disconnect();
            
            //return JSON from HTTP response
            return buffer.toString();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
        	
        	//make sure to close connections even if exception occurs before the code above gets to it
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }
 
        return null;
                 
    }

    /**
     * Parse the Json and convert it into a List of JsonWeather
     * objects.
     * @throws JSONException 
     */
    private static List<JsonWeather> parseJson(String inputJson) throws JSONException {
    	
        // TODO -- you fill in here.
    	
    	return parseJsonWeatherArray(new JSONObject(inputJson));
    }

    /**
     * Parse a Json object and convert it into a List of JsonWeather
     * objects.
     * @throws JSONException 
     */
    private static List<JsonWeather> parseJsonWeatherArray(JSONObject jObj) throws JSONException {

        // TODO -- you fill in here.
    	
    	//I'm just going to assume that there is only one result
    	ArrayList<JsonWeather> weatherList = new ArrayList<JsonWeather>();
    	weatherList.add(parseJsonWeather(jObj));
    	
    	return weatherList;
    }

    /**
     * Parse a Json object and return a JsonWeather object.
     */
    private static JsonWeather parseJsonWeather(JSONObject jObj) 
        throws JSONException {

        // TODO -- you fill in here.
    	
    	// process a JsonWeather object
    	return new JsonWeather(
    			parseSys(jObj),
                jObj.getString(JsonWeather.base_JSON),
                parseMain(jObj),
                parseWeathers(jObj),
                parseWind(jObj),
                jObj.getLong(JsonWeather.dt_JSON),
                jObj.getLong(JsonWeather.id_JSON),
                jObj.getString(JsonWeather.name_JSON),
                jObj.getLong(JsonWeather.cod_JSON)
    			);
    }
    
    /**
     * Parse a Json object and return a List of Weather objects.
     */
    private static List<Weather> parseWeathers(JSONObject jObj) throws JSONException {
    	
        // TODO -- you fill in here.
    	
    	// get the weather array
    	JSONArray jArr = jObj.getJSONArray(JsonWeather.weather_JSON);
    	
    	ArrayList<Weather> weatherList = new ArrayList<Weather>();
    	for (int i = 0; i < jArr.length(); i++){
    		
    		JSONObject weatherObj = jArr.getJSONObject(i);
    		weatherList.add(parseWeather(weatherObj));
    	}
    	
    	return weatherList;
    }

    /**
     * Parse a Json object and return a Weather object.
     */
    private static Weather parseWeather(JSONObject jObj) throws JSONException {
    	
        // TODO -- you fill in here.   
    	
    	Weather weather = new Weather();
    	if (jObj.has(Weather.description_JSON))
    		weather.setDescription(jObj.getString(Weather.description_JSON));
    	if (jObj.has(Weather.icon_JSON))
    		weather.setIcon(jObj.getString(Weather.icon_JSON));
    	if (jObj.has(Weather.id_JSON))
    		weather.setId(jObj.getLong(Weather.id_JSON));
    	if (jObj.has(Weather.main_JSON))
    		weather.setMain(jObj.getString(Weather.main_JSON));
    	
    	return weather;
    }
    
    /**
     * Parse a Json object and return a Main Object.
     * @throws JSONException 
     */
    private static Main parseMain(JSONObject jObj) throws JSONException {
    	
        // TODO -- you fill in here. 
    	
    	JSONObject mainObj = jObj.getJSONObject(JsonWeather.main_JSON);
    	
    	Main main = new Main();
    	if (mainObj.has(Main.grndLevel_JSON))
    		main.setGrndLevel(mainObj.getDouble(Main.grndLevel_JSON));
    	if (mainObj.has(Main.humidity_JSON))
    		main.setHumidity(mainObj.getLong(Main.humidity_JSON));
    	if (mainObj.has(Main.pressure_JSON))
    		main.setPressure(mainObj.getDouble(Main.pressure_JSON));
    	if (mainObj.has(Main.seaLevel_JSON))
    		main.setSeaLevel(mainObj.getDouble(Main.seaLevel_JSON));
    	if (mainObj.has(Main.temp_JSON))
    		main.setTemp(mainObj.getDouble(Main.temp_JSON));
    	if (mainObj.has(Main.tempMax_JSON))
    		main.setTempMax(mainObj.getDouble(Main.tempMax_JSON));
    	if (mainObj.has(Main.tempMin_JSON))
    		main.setTempMin(mainObj.getDouble(Main.tempMin_JSON));
    	
    	return main;
    }

    /**
     * Parse a Json object and return a Wind Object.
     */
    private static Wind parseWind(JSONObject jObj) throws JSONException {
    	
        // TODO -- you fill in here.    
    	
    	JSONObject windObj = jObj.getJSONObject(JsonWeather.wind_JSON);
    	
    	Wind wind = new Wind();
    	if (windObj.has(Wind.speed_JSON))
    		wind.setSpeed(windObj.getDouble(Wind.speed_JSON));
    	if (windObj.has(Wind.deg_JSON))
    		wind.setDeg(windObj.getDouble(Wind.deg_JSON));
    	
    	return wind;
    }

    /**
     * Parse a Json object and return a Sys Object.
     */
    private static Sys parseSys(JSONObject jObj)
    		 throws JSONException {
    	
        // TODO -- you fill in here. 
    	
    	JSONObject sysObj = jObj.getJSONObject(JsonWeather.sys_JSON);

    	Sys sys = new Sys();
    	if (sysObj.has(Sys.country_JSON))
    		sys.setCountry(sysObj.getString(Sys.country_JSON));
    	if (sysObj.has(Sys.message_JSON))
    		sys.setMessage(sysObj.getDouble(Sys.message_JSON));
    	if (sysObj.has(Sys.sunrise_JSON))
    		sys.setSunrise(sysObj.getLong(Sys.sunrise_JSON));
    	if (sysObj.has(Sys.sunset_JSON))
    		sys.setSunset(sysObj.getLong(Sys.sunset_JSON));
    	
    	return sys;
    }
}
