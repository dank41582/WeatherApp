package vandy.mooc.weatherappdmk2;

import java.util.HashMap;
import java.util.List;

import vandy.mooc.aidl.WeatherData;

//cache weather data results for 10 seconds
public class WeatherCacheImpl implements WeatherCache {
	
	private static int TEN_SECONDS = 10000;
	
	private HashMap<String,WeatherDataResults> weatherDataMap = new HashMap<String,WeatherDataResults>();
	

	//add the weather data results to the cache
	@Override
	public void release(String cityName, List<WeatherData> weatherDataIn) {
		
		weatherDataMap.put(cityName, new WeatherDataResults(weatherDataIn, System.currentTimeMillis()));
	}

	//return weather data results from cache, if they exist; if data is old, delete it and return null
	@Override
	public List<WeatherData> acquire(String cityName) {
		
		if (!weatherDataMap.containsKey(cityName))
			return null;
		
		WeatherDataResults weatherDataResults = weatherDataMap.get(cityName);
		
		if (weatherDataResults == null){
			return null;
		}
		//if cache is old, delete it
		else if (weatherDataResults.getTimeCreated() < (System.currentTimeMillis() - TEN_SECONDS)){
			weatherDataMap.remove(cityName);
			return null;
		}
		
		return weatherDataResults.getWeatherDataResults();
	}
}