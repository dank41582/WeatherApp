package vandy.mooc.weatherappdmk2;

import java.util.List;

import vandy.mooc.aidl.WeatherCall;
import vandy.mooc.aidl.WeatherData;
import vandy.mooc.aidl.WeatherRequest;
import vandy.mooc.aidl.WeatherResults;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends LifecycleLoggingActivity {
	
	/**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();
    
    private static String DEFAULT_CITY = "Chicago,IL";
    
    private EditText mCityEditText;
    
    private TextView mOutputText;
    
    private String requestedCityName = "";
    
    //cache that holds map of results
    private final WeatherCacheImpl weatherCache = new WeatherCacheImpl();
    
    
    /**
     * The AIDL Interface that's used to make twoway calls to the
     * DownloadServiceSync Service.  This object plays the role of
     * Requestor in the Broker Pattern.  If it's null then there's no
     * connection to the Service.
     */
    WeatherCall mDownloadCall;
     
    /**
     * The AIDL Interface that we will use to make oneway calls to the
     * DownloadServiceAsync Service.  This plays the role of Requestor
     * in the Broker Pattern.  If it's null then there's no connection
     * to the Service.
     */
    WeatherRequest mDownloadRequest;
    
    
  //the following ServiceConnections are used to connect to the sync and async services:
    
    /** 
     * This ServiceConnection is used to receive results after binding
     * to the DownloadServiceSync Service using bindService().
     */
    ServiceConnection mServiceConnectionSync = new ServiceConnection() {
            /**
             * Cast the returned IBinder object to the DownloadCall
             * AIDL Interface and store it for later use in
             * mDownloadCall.
             */
            @Override
            public void onServiceConnected(ComponentName name,
                                           IBinder service) {
            	Log.d(TAG, "ComponentName: " + name);
                // Call the generated stub method to convert the
                // service parameter into an interface that can be
                // used to make RPC calls to the Service.
                mDownloadCall =
                    WeatherCall.Stub.asInterface(service);
            }

            /**
             * Called if the remote service crashes and is no longer
             * available.  The ServiceConnection will remain bound,
             * but the service will not respond to any requests.
             */
            @Override
            public void onServiceDisconnected(ComponentName name) {
            	
            	//setting it to null kills the connection
                mDownloadCall = null;
            }
        };
     
    /** 
     * This ServiceConnection is used to receive results after binding
     * to the DownloadServiceAsync Service using bindService().
     * 
     * *Only difference compared to sync version (above) is that this uses DownloadRequest instead of DownloadCall
     */
    ServiceConnection mServiceConnectionAsync = new ServiceConnection() {
            /**
             * Cast the returned IBinder object to the DownloadRequest
             * AIDL Interface and store it for later use in
             * mDownloadRequest.
             */
            @Override
		public void onServiceConnected(ComponentName name,
                                               IBinder service) {
                // Call the generated stub method to convert the
                // service parameter into an interface that can be
                // used to make RPC calls to the Service.
                mDownloadRequest =
                    WeatherRequest.Stub.asInterface(service);
            }

            /**
             * Called if the remote service crashes and is no longer
             * available.  The ServiceConnection will remain bound,
             * but the service will not respond to any requests.
             */
            @Override
		public void onServiceDisconnected(ComponentName name) {
            	
            	//setting it to null kills the connection
                mDownloadRequest = null;
            }
        };
    
    /**
     * The implementation of the DownloadResults AIDL
     * Interface. Should be passed to the DownloadBoundServiceAsync
     * Service using the DownloadRequest.downloadImage() method.
     * 
     * This implementation of DownloadResults.Stub plays the role of
     * Invoker in the Broker Pattern.
     */
    WeatherResults.Stub mDownloadResults = new WeatherResults.Stub() {
    	
            /**
             * Called when the DownloadServiceAsync finishes obtaining
             * the results from the GeoNames Web service.  Use the
             * provided String to display the results in a TextView.
             */
            @Override
            public void sendResults(final List<WeatherData> results) throws RemoteException {
                // Create a new Runnable whose run() method displays
                // the bitmap image whose pathname is passed as a
                // parameter to sendPath().  Please use
                // displayBitmap() defined in DownloadBase.
                final Runnable displayRunnable = new Runnable() {
                        public void run() {
                        	
                        	//add weather data to cache
                        	weatherCache.release(requestedCityName, results);
                        	
                        	displayResults(results);
                        }
                    };

                //since stub is run in background thread, we need to run this in the UI thread
                runOnUiThread(displayRunnable);
            }
        };
        
        

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		mCityEditText = (EditText) findViewById(R.id.city);
		
		mOutputText = (TextView) findViewById(R.id.outputText);
	}
	

    /**
     * Hook method called when the DownloadActivity becomes visible to
     * bind the Activity to the Services.
     */
    @Override
    public void onStart() {
    	super.onStart();
    	
    	// Bind this activity to the DownloadBoundService* Services if
    	// they aren't already bound Use mBoundSync/mBoundAsync
    	
    	if(mDownloadCall == null) 
            bindService(DownloadBoundServiceSync.makeIntent(this), 
                        mServiceConnectionSync, 
                        BIND_AUTO_CREATE);
    	
    	if(mDownloadRequest == null)
            bindService(DownloadBoundServiceAsync.makeIntent(this), 
                        mServiceConnectionAsync, 
                        BIND_AUTO_CREATE);
    }
    
    /**
     * Hook method called when the DownloadActivity becomes completely
     * hidden to unbind the Activity from the Services.
     */
    @Override
    public void onStop() {
    	super.onStop();
    	
    	// Unbind the Sync/Async Services if they are bound. Use
    	// mBoundSync/mBoundAsync
    	
    	if(mDownloadCall != null) 
            unbindService(mServiceConnectionSync);
    	if(mDownloadRequest != null) 
            unbindService(mServiceConnectionAsync);
    }
    
    /**
     * This method is called when a user presses a button (see
     * res/layout/activity_download.xml)
     * 
     * user can choose between clicking "sync" and "async" buttons
     */
    public void runService(View view) {
    	
        requestedCityName = getCityNameFromInput(view);

        hideKeyboard();
        
        //check to see if the results for this city are already in the cache
        List<WeatherData> weatherDataResultsFromCache = weatherCache.acquire(requestedCityName);
        
        //if cache results exist, display those
        if (weatherDataResultsFromCache != null){
        	displayResults(weatherDataResultsFromCache);
        	return;
        }

    	switch(view.getId()) {
    	
        case R.id.button1:
        	
            if (mDownloadCall != null) {
            	
                Log.d(TAG,
                      "Calling twoway DownloadServiceSync.downloadImage()");
                
                /** 
                 * Define an AsyncTask instance to avoid blocking the UI Thread. 
                 * */
                new AsyncTask<String, Void, List<WeatherData>>() {
                	
                    /**
                     * Runs in a background thread.
                     */
                    @Override
                    protected List<WeatherData> doInBackground(String... params) {
                    	
                        try {
                        	
                        	Log.d(TAG, "City requested: " + params[0]);
                        	
                        	//send request to service to download image
                        	//the following method call blocks, so you may want to run this in a separate thread (as is done here with AysncTask)
                        	List<WeatherData> results = mDownloadCall.getCurrentWeather(params[0]);
                        	
                        	//add weather data to cache
                        	weatherCache.release(params[0], results);
                        	
                            return results;
                            
                        } catch(RemoteException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                    /**
                     * Runs in the UI Thread.
                     */
                    @Override
                    protected void onPostExecute(List<WeatherData> result) {
                    	
                    	//display results
                        if (result != null) 
                            displayResults(result);
                    }
                    
                }.execute(requestedCityName);
            }
            break;

        case R.id.button2:
        	
            if(mDownloadRequest != null) {
            	
                try {
                    Log.d(TAG,
                          "Calling oneway DownloadServiceAsync.getCurrentWeather()");
                    
                    Log.d(TAG, "City requested: " + requestedCityName);

                    // Request service to download image by 
                    // oneway call to downloadImage() via mDownloadRequest, passing in
                    // the appropriate Uri and a callback object that will receive the result.
                    // Does not block.
                    mDownloadRequest.getCurrentWeather(requestedCityName,
                                                   mDownloadResults);
                } catch(RemoteException e) {
                    e.printStackTrace();
                }
            }
            break;
        }
    }
	
	private String getCityNameFromInput(View view){
		
		String cityName = mCityEditText.getText().toString();
		if (cityName.isEmpty())
			cityName = DEFAULT_CITY;
		
		return cityName;
	}
	
	private void displayResults(List<WeatherData> results){
		
		String resultsString = "";
		
		if (results == null || results.size() < 1){
		
			resultsString = "City not found.";
		}
		else{
			
			for(WeatherData data : results){
				
				resultsString += data.toString() + "\r\n";
			}
		}
		
		Log.d(TAG, resultsString);
		
		mOutputText.setText(resultsString);
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
    /**
     * Hide the keyboard after a user has finished typing in the city.
     */
    protected void hideKeyboard() {
        InputMethodManager mgr =
            (InputMethodManager) getSystemService
            (Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(mCityEditText.getWindowToken(),
                                    0);
    }
    
    
//	private String test(String cityName){
//	
//	
//	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//	StrictMode.setThreadPolicy(policy);
//	
//	Log.d(TAG, "Start...");
//	
//	String weatherDataJson = WeatherJSONParser.getWeatherData(cityName);
//	
//	if (weatherDataJson != null)
//		Log.d(TAG, weatherDataJson);
//	
//	String results = "";
//	
//	//check to see if city was found
//	if (weatherDataJson.equals("{\"message\":\"Error: Not found city\",\"cod\":\"404\"}")){
//		
//		results = "City not found.";
//	}
//	else{
//		
//		//parse the json, get java objects and print results in readable format
//    	JSONObject rootJsonObject;
//		try {
//			rootJsonObject = new JSONObject(weatherDataJson);
//			
//			JsonWeather jsonWeather = WeatherJSONParser.parseJsonWeather(rootJsonObject);
//			
//			results = jsonWeather.toString();
//			
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//	
//	return results;
//}
}
