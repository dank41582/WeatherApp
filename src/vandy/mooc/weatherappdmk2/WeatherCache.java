package vandy.mooc.weatherappdmk2;

import java.util.List;

import vandy.mooc.aidl.WeatherData;

//use to cache weather data results for set period of time
public interface WeatherCache {
	public void release(String cityName, List<WeatherData> weatherDataIn);
	public List<WeatherData> acquire(String cityName);
}