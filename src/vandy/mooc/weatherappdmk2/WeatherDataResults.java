package vandy.mooc.weatherappdmk2;

import java.util.List;

import vandy.mooc.aidl.WeatherData;

//for use with caching weather data results
public class WeatherDataResults {
	
	//weather data results
	private List<WeatherData> weatherDataResults;
	
	//time weather data results were created in our system
	private long timeCreated;
	
	public WeatherDataResults(List<WeatherData> weatherDataResults, long timeCreated){
		this.weatherDataResults = weatherDataResults;
		this.timeCreated = timeCreated;
	}
	
	public List<WeatherData> getWeatherDataResults() {
		return weatherDataResults;
	}
	
	public void setWeatherDataResults(List<WeatherData> weatherDataResults) {
		this.weatherDataResults = weatherDataResults;
	}
	
	public long getTimeCreated() {
		return timeCreated;
	}
	
	public void setTimeCreated(long timeCreated) {
		this.timeCreated = timeCreated;
	}
	
	
}